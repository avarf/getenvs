# getenvs

A small package for getting the environment values and also providing a default value which if the environment value hasn't been set the package returns the default value.